import os
import torch
import torchvision
import csv

from PIL import Image

from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
import torchvision.models as models

import matplotlib.pyplot as plt
import pandas as pd

LABEL_NAMES = {'background':0, 'kart':1, 'pickup':2, 'nitro':3, 'bomb':4, 'projectile':5}

LABEL_=['background','kart','pickup','nitro','bomb','projectile']

class SuperTuxDataset(Dataset):
    def __init__(self, image_path,data_transforms=None):
        """
        Your code here
        Hint: Use the python csv library to parse labels.csv
        """
        self.image_path = image_path
        self.data_transforms = data_transforms

        # read labels and convert label to index
        self.label_df = pd.read_csv(os.path.join(image_path, 'labels.csv'))
        self.label_df['label_idx'] = self.label_df['label'].map(LABEL_NAMES)

    def __len__(self):
        """
        return the size of the dataset, i.e., the number of samples
        """
        return len(self.label_df)

    def __getitem__(self, idx):
        """
        return a tuple: img, label
        """
        # get label
        label = self.label_df.iloc[idx]['label_idx']

        # get image name from dataframe
        img_name = os.path.join(self.image_path,
                                self.label_df.iloc[idx]['file'])
        
        # Open image and convert to tensor
        img_pil = Image.open(img_name)
        img = torchvision.transforms.ToTensor()(img_pil)

        if self.data_transforms is not None:
            img, target = self.data_transforms(img, target)

        return img, label
        

def test_logging(train_logger, valid_logger):

    """
    Your code here.
    Finish logging the dummy loss and accuracy
    Log the loss every iteration, the accuracy only after each epoch
    Make sure to set global_step correctly, for epoch=0, iteration=0: global_step=0
    Call the loss 'loss', and accuracy 'accuracy' (no slash or other namespace)
    """

    # This is a strongly simplified training loop
    for epoch in range(10):
        torch.manual_seed(epoch)
        for iteration in range(20):
            dummy_train_loss = 0.9**(epoch+iteration/20.)
            dummy_train_accuracy = epoch/10. + torch.randn(10)
            train_logger.add_scalar('loss', dummy_train_loss, iteration)
        train_logger.add_scalar('accuracy', dummy_train_accuracy.mean(), epoch)
        torch.manual_seed(epoch)
        for iteration in range(10):
            dummy_validation_accuracy = epoch / 10. + torch.randn(10)
        valid_logger.add_scalar('accuracy', dummy_validation_accuracy.mean(), iteration)

class ClassificationLoss(torch.nn.Module):
    def forward(self, input, target):
        """
        Compute mean(-log(softmax(input)_label))
        @input:  torch.Tensor((B,C)), where B = batch size, C = number of classes
        @target: torch.Tensor((B,), dtype=torch.int64)
        @return:  torch.Tensor((,))
        Hint: Don't be too fancy, this is a one-liner
        """
        nll_loss = torch.nn.NLLLoss()
        return nll_loss(torch.nn.functional.log_softmax(input, dim=1), target)


class CNNClassifier(torch.nn.Module):
    def __init__(self):
        """
        Your code here
        """
        super(CNNClassifier, self).__init__()
        model = models.densenet161(pretrained=True)

        for param in model.parameters():
            param.requires_grad = False

        classifier_input = model.classifier.in_features
        num_labels = 6
        classifier = torch.nn.Sequential(torch.nn.Linear(classifier_input, 1024),
                                torch.nn.ReLU(),
                                torch.nn.Linear(1024, 512),
                                torch.nn.ReLU(),
                                torch.nn.Linear(512, num_labels))

        model.classifier = classifier

        self.model = model

    def forward(self, x):
        """
        Your code here
        @x: torch.Tensor((B,3,64,64))
        @return: torch.Tensor((B,6))
        """
        return self.model(x)
